package main
import (
    "fmt"
    "os"
    "os/exec"
    "bufio"
    "strings"
    "bytes"
    "io"
    "runtime"
)

func main() {
    
    reader := bufio.NewReader(os.Stdin)
    encyptedStr, _ := reader.ReadString('\n')
    decyptedStr := ""

    os.Stderr.WriteString("Now Loading, Please Wait ...\n")

    for _, char := range encyptedStr {
        C := string(char)

        switch C {

            case "랕": decyptedStr += " "
            case "랑": decyptedStr += "`"
            case "대": decyptedStr += "~"
            case "너": decyptedStr += "\t"
            case "독": decyptedStr += "\r"
            case "접": decyptedStr += "\n"
        
            case "바": decyptedStr += "!"
            case "댜": decyptedStr += "@"
            case "득": decyptedStr += "#"
            case "쟟": decyptedStr += "$"
            case "더": decyptedStr += "%"
            case "언": decyptedStr += "^"
            case "닫": decyptedStr += "&"
            case "준": decyptedStr += "*"
            case "저": decyptedStr += "("
            case "걾": decyptedStr += ")"
            case "퓨": decyptedStr += "-"
            case "곡": decyptedStr += "_"
            case "서": decyptedStr += "+"
            case "버": decyptedStr += "="
            case "갇": decyptedStr += "["
            case "잗": decyptedStr += "{"
            case "거": decyptedStr += "]"
            case "샤": decyptedStr += "}"
            case "아": decyptedStr += ":"
            case "숃": decyptedStr += ";"
            case "긎": decyptedStr += "\""
            case "긒": decyptedStr += "'"
            case "다": decyptedStr += "|"
            case "리": decyptedStr += "\\"
            case "군": decyptedStr += "<"
            case "재": decyptedStr += ","
            case "달": decyptedStr += ">"
            case "잳": decyptedStr += "."
            case "래": decyptedStr += "?"
            case "쟈": decyptedStr += "/"
        
            case "랮": decyptedStr += "1"
            case "걸": decyptedStr += "2"
            case "론": decyptedStr += "3"
            case "롸": decyptedStr += "4"
            case "먼": decyptedStr += "5"
            case "오": decyptedStr += "6"
            case "랃": decyptedStr += "7"
            case "조": decyptedStr += "8"
            case "몾": decyptedStr += "9"
            case "로": decyptedStr += "0"
        
            case "도": decyptedStr += "a"
            case "랴": decyptedStr += "b"
            case "촌": decyptedStr += "c"
            case "파": decyptedStr += "d"
            case "렂": decyptedStr += "e"
            case "러": decyptedStr += "f"
            case "젇": decyptedStr += "g"
            case "랻": decyptedStr += "h"
            case "내": decyptedStr += "i"
            case "쟏": decyptedStr += "j"
            case "야": decyptedStr += "k"
            case "탸": decyptedStr += "l"
            case "갲": decyptedStr += "m"
            case "됴": decyptedStr += "n"
            case "추": decyptedStr += "o"
            case "텨": decyptedStr += "p"
            case "교": decyptedStr += "q"
            case "와": decyptedStr += "r"
            case "옴": decyptedStr += "s"
            case "라": decyptedStr += "t"
            case "ㅘ": decyptedStr += "u"
            case "고": decyptedStr += "v"
            case "큐": decyptedStr += "w"
            case "뵻": decyptedStr += "x"
            case "펵": decyptedStr += "y"
            case "슈": decyptedStr += "z"
        
            case "두": decyptedStr += "A"
            case "드": decyptedStr += "B"
            case "슉": decyptedStr += "C"
            case "궄": decyptedStr += "D"
            case "뎌": decyptedStr += "E"
            case "넝": decyptedStr += "F"
            case "졾": decyptedStr += "G"
            case "처": decyptedStr += "H"
            case "혗": decyptedStr += "I"
            case "폄": decyptedStr += "J"
            case "럏": decyptedStr += "K"
            case "펴": decyptedStr += "L"
            case "듀": decyptedStr += "M"
            case "쳫": decyptedStr += "N"
            case "뎣": decyptedStr += "O"
            case "퍄": decyptedStr += "P"
            case "젿": decyptedStr += "Q"
            case "쳦": decyptedStr += "R"
            case "쳐": decyptedStr += "S"
            case "쥬": decyptedStr += "T"
            case "패": decyptedStr += "U"
            case "피": decyptedStr += "V"
            case "듈": decyptedStr += "W"
            case "차": decyptedStr += "X"
            case "닿": decyptedStr += "Y"
            case "덫": decyptedStr += "Z"
            default: decyptedStr += C

        }
    }

    var stdBuffer bytes.Buffer
    mw := io.MultiWriter(os.Stdout, &stdBuffer)

    // Check Platform, Prepend TERM type
    cmdline := ""
    if runtime.GOOS == "windows" {
        cmdline = "TERM=linux " + decyptedStr
    } else {
        cmdline = decyptedStr
    }

    // Redirect Stdin, Stdout, Stderr
    shell := exec.Command("sh")
    shell.Stdin = strings.NewReader(cmdline)
    shell.Stdout = mw
    shell.Stderr = mw

    // Execute the command
    if err := shell.Run(); err != nil {
        fmt.Println(err)
    }
}