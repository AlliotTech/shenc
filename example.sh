#!/bin/sh
#__可选预编译下载源__
SHDEC_DL_PREFIX="https://gitlab.com/dfc643/shenc/-/raw/master/shdec"
#SHDEC_DL_PREFIX="http://dfc643.9966.org:8081/shdec"

#__如本脚本没有为您的平台优化__
#__保存脚本至本地自行修改使用__

#__预编译SHDEC__
#shdec-darwin-amd64
#shdec-freebsd-386
#shdec-freebsd-amd64
#shdec-freebsd-arm5
#shdec-freebsd-arm6
#shdec-freebsd-arm7
#shdec-linux-386
#shdec-linux-amd64
#shdec-linux-arm
#shdec-linux-arm5
#shdec-linux-arm6
#shdec-linux-arm64
#shdec-linux-arm7
#shdec-linux-mips
#shdec-linux-mips64
#shdec-linux-mips64le
#shdec-linux-mipsle
#shdec-netbsd-386
#shdec-netbsd-amd64
#shdec-netbsd-arm5
#shdec-netbsd-arm6
#shdec-netbsd-arm7
#shdec-openbsd-386
#shdec-openbsd-amd64
#shdec-windows-386.exe
#shdec-windows-amd64.exe

#__区分Windows和Linux平台__
if [ "$OS" = "Windows_NT" ]; then
    TEMP_PATH=$TEMP
    SHDEC_NAME="shdec-windows-386.exe"
    SHDEC_SAVE="shdec.exe"
else
    TEMP_PATH="/tmp"
    SHDEC_SAVE="shdec"
    
    #__非Windows平台__
    #__判断硬件架构__
    case `uname -m` in
    "x64" | "x86_64" | "amd64" | "ia64" | "i686-64")
        SHDEC_NAME="shdec-linux-amd64"
        ;;
    "i386" | "i486" | "i586" | "i686" | "i686-AT386" | \
    "x86" | "x86pc" | "i86pc")
        SHDEC_NAME="shdec-linux-386"
        ;;
    "armv6l" | "arm")
        SHDEC_NAME="shdec-linux-arm6"
        ;;
    "armv7l")
        SHDEC_NAME="shdec-linux-arm7"
        ;;
    "aarch64" | "aarch64_ilp32" | "aarch64_be" | \
    "arm64" | "armv8b" | "armv8l")
        SHDEC_NAME="shdec-linux-arm64"
        ;;
    "mips64")
        SHDEC_NAME="shdec-linux-mips64"
        ;;
    #__MIPS平台需要特别判断__
    #__MTK多为MIPSEL而非MIPS__
    "mips")
        CPU_MODEL=`cat /proc/cpuinfo | grep "model" | head -n1 | awk -F': ' '{print $2}'`
        FIND_1004K=`echo $CPU_MODEL | grep " 1004K"`
        FIND_24K=`echo $CPU_MODEL | grep " 24K"`
        FIND_74K=`echo $CPU_MODEL | grep " 74K"`
        FIND_4K=`echo $CPU_MODEL | grep " 4K"`
        if [ "$FIND_1004K" != "" ] || [ "$FIND_24K" != "" ] || [ "$FIND_74K" != "" ] || [ "$FIND_4K" != "" ]
        then
            SHDEC_NAME="shdec-linux-mipsle"
        else
            SHDEC_NAME="shdec-linux-mips"
        fi
        ;;
    *)
        echo "请手动修改脚本来完善对您设备的支持！"
        exit 127
        ;;
    esac
fi

#__如果不存在SHDEC则下载__
if [ ! -f $TEMP_PATH/$SHDEC_SAVE ]; then
    echo "未检测到 SHDEC 二进制文件，开始下载 ..."
    curl -kL "${SHDEC_DL_PREFIX}/${SHDEC_NAME}" > $TEMP_PATH/$SHDEC_SAVE
    chmod 777 $TEMP_PATH/$SHDEC_SAVE
fi

#__执行程序__
curl -skL https://gitlab.com/snippets/1956931/raw | $TEMP_PATH/$SHDEC_SAVE
